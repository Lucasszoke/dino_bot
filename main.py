import time

from PIL import ImageGrab
import pyautogui

first_x = 270
time_to_press = 0.25


def capture_screen():
	screen = ImageGrab.grab()
	return screen


def detect_cactus(screen):
	aux_color = screen.getpixel((first_x, 215))
	for x in range(first_x, first_x+20):
		for y in range(375, 450):
			color = screen.getpixel((x, y))
			if color != aux_color:
				return True
			else:
				aux_color = color


def jump():
	global first_x
	global time_to_press
	if first_x <= 360:
		first_x += 1
	pyautogui.press('up')
	time.sleep(time_to_press)
	pyautogui.keyDown('down')
	time.sleep(0.03)
	pyautogui.keyUp('down')
	if time_to_press >= 0.08:
		time_to_press -= 0.001


jumps = 0
print('Start in 3 seconds...')
time.sleep(3)
pyautogui.press('space')
while True:
	screen = capture_screen()
	if detect_cactus(screen):
		jumps += 1
		jump()
		print(jumps)
